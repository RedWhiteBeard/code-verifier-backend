- Dependencias
 . "dotenv": "^16.0.0", => Se utiliza para tener a nivel de proyecto un archivo de entorno (.env) al que poder hacer consultas y pedir determinada información privada que no se subira a github.

 . "express": "^4.17.3"  => Express nos permite crear una aplicacion de back-end con Node.

- Dependencias Desarrollo

    . "typescript": "^4.6.3", => Genera un archivo tsconfig.json donde nos importa el tsc que es la herramienta que se utiliza para transpilar el codigo. Coge el código de typescript y genera una solución de javascript.

    . "@types/express": "^4.17.13", 
    . "@types/jest": "^27.4.1", 
    . "@types/node": "^17.0.23", 
    . "@typescript-eslint/eslint-plugin": "^5.17.0", 
    . "@typescript-eslint/parser": "^5.17.0", 
    . "ts-jest": "^27.1.4", 
    . "ts-node": "^10.7.0", 
    => Permite entender el codigo typescript a las distintas dependencias.

    . "concurrently": "^7.1.0", => Permite ejecutar comandos de forma concurrente por lo que es mucho más sencillo ejecutar varios comandos de forma simultanea.

    . "eslint": "^7.32.0", 
    . "eslint-config-standard": "^16.0.3", 
    . "eslint-plugin-import": "^2.25.4", 
    . "eslint-plugin-node": "^11.1.0", 
    . "eslint-plugin-promise": "^5.2.0", 
    . "supertest": "^6.2.2",
    => Pone reglas en nuestro proyecto y empezar a programar de forma correcta desde el principio reglas a nivel de programación y código limpio.

    . "jest": "^27.5.1", => Configura las preferencias para elegir la forma de implementar los test.

    . "nodemon": "^2.0.15", => Hace que cualquier modificación salvada en el proyecto genere un reinicio de la aplicacion de forma automantica y así poder visualizar los cambios.

    . "serve": "^13.0.2", => Servir nuestro coverage a nivel de web y asi poder visualizar nuestro coverage.

    . "webpack": "^5.71.0", 
    . "webpack-cli": "^4.9.2", 
    . "webpack-node-externals": "^3.0.0", 
    . "webpack-shell-plugin": "^0.5.0", 
    => Nos permite empaquetar la solucion y que esra sea más ligera. Cli se utiliza para ejecutarlo mientras que Node-externals y shell-plugin nos sirven para hacer la configuracion de webpack.

- Scripts de NPM:
. npm init 
Genera unproyecto Node, para ello generamos un package Json que contiene todas las dependencias del proyecto.

.npm i --save -D
.npmi express dotenv
Express nos permite crear una aplicación de back-end con Node.
dotenv, se utiliza para tener a nivel de proyecto un archivo de entorno(.env)al que poder hacer consultas y pedir determinada información privada que ademas no se subira a nuestro github.

. npm i -D nodemon
Hace que cualquier modificación en el proyecto genere un reinicio de la aplicación y así visualizar los cambios.

.npm i -D typescript @types/express @types/node
Nos genera un archivo tsconfig.json donde nos importa el tsc que es la herramienta que se utiliza para transpilar el codigo. Coge el codigo de typescript y genera una solución de javascript.

. npx tsc --init 
Nos genera un archivo de ts con todos los elementos que se pueden comentar y descomentar.

. npm i -D concurrently 
Permite ejecutar comandos de forma concurrente por lo que es mucho mas sencillo ejecutar varios comandos de forma simultanea.

. npm run build 
Nos genera la carpeta dist donde tenemos el archivo index.js.map donde se hace la referencia al origen y por otro lado el index.js

. npm run start
Inicia la aplicación.

. npm run dev
Inicia la aplicación recargando la aplicación despues de guardar cualquier cambio en el proyecto para que se refleje en la vista.

. npm i -D webpack webpack-cli webpack-node-externals webpack-shell-plugins
Nos permite empaquetar la solución y que esta sea mas ligera.
Cli se utiliza para ejecutarlo.
Webpack-node-externals webpack-shell-plugins, nos sirve para hacer la configuración de webpack

. npm i -D eslint jest ts-jest @types/jest supertest
Para poner reglas en nuestro proyecto y empezar a programar de forma correcta desde el principio con codigo limpio y mantenible.

. npx eslint --init 
Configura eslint.

. npx jest --init
Nos pide configurar las preferencias de jest para elegir la forma de implementar los test.

. npm i -D ts-node 
Configurar la transpilacion en typescript desde node.

. npm run test 
Ejecuta jest

. npm install -D serve 
Servir nuestro coverage a nivel de web y asi poder visualizar nuestro coverage.

- Variables de entorno.
Variable de entorno  { PORT } para seleccionar el puerto de escucha.

