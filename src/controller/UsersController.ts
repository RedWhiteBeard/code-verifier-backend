import { Delete, Get, Post, Put, Query, Route, Tags } from "tsoa";
import { IUserController } from "./interfaces";
import { LogSuccess, LogError, LogWarning } from "../utils/logger";

// ORM - Users Collection
import { getAllUsers, getUserByID, deleteUserByID, createUser, updateUserByID,   } from "../domain/orm/User.orm";




@Route("/api/users")
@Tags("UserController")
export class UserController implements IUserController {
    deleteUser(id?: string): Promise<any> {
        throw new Error("Method not implemented.");
    }
    
    /**
     * Endpoint to retrieve the Users in the Collection "Users" of DB
     * @param {string} id Id of user to retrieve (optional)
     * @returns All users o user found by iD
     */
    @Get("/")
    public async getUsers(@Query()id?: string): Promise<any> {

        let response: any = '';

        if(id){
            LogSuccess(`[/api/users] Get User By ID: ${id} `);
            response = await getUserByID(id);
        }else {
            LogSuccess('[/api/users] Get All User Request')
            response = await getAllUsers();  
            // TODO: remove passwords from response         
        }
        return response;           
    }

    /**
     * Endpoint to delete the Users in the Collection "Users" of DB
     * @param {string} id Id of user to delete (optional)
     * @returns message informing if deletion was correct
     */
    @Delete("/")
    public async deleteUsers(@Query()id?: string): Promise<any> {
        

        let response: any = '';

        if(id){
            LogSuccess(`[/api/users] Delete User By ID: ${id} `);
            
            await deleteUserByID(id).then((r) => {
                response = {
                    status: 204,
                    message: `User with id ${id} deleted successfully`
                }
            });
        }else {
            LogWarning('[/api/users] Delete User Request WITHOUT ID')
            response = {
                message: 'Please, provide an ID to remove from database'
            }           
        }
        
        return response;        
    }

    
    @Put("/")
    public async updateUser(@Query()id: string, user: any): Promise<any> {

        let response: any = '';

        if(id){
            LogSuccess(`[/api/users] Update User By ID: ${id} `);
            await updateUserByID(id, user).then((r) => {
                response = {
                    status: 204,
                    message: `User with id ${id} updated successfully`
                }
            })
        }else {
            LogWarning('[/api/users] Update User Request WITHOUT ID')
            response = {
                message: 'Please, provide an ID to update an existing user'
            }           
        }

        return response;        
    }

    

}


