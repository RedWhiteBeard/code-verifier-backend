import express, { Request, Response } from "express";
import { UserController } from "../controller/UsersController";
import { LogInfo } from "../utils/logger";

// Body Parser to read BODY requests
import bodyParser from "body-parser";

let jsonParser = bodyParser.json();

// Router from express
let usersRouter = express.Router();

// http://localhost:8000/api/users?id=6256bf6336b823c6f970330e
usersRouter.route('/')
    // GET:
    .get(async (req: Request, res: Response) => {
        // Obtain a Query param (ID)
        let id: any = req?.query?.id;
        LogInfo(`Query Param: ${id}`);
        // Controller Instance to excute method
        const controller: UserController = new UserController();
        // Obtain Reponse
        const response: any = await controller.getUsers(id)
        // Send to the client the response
        return res.status(200).send(response);
    })
    // DELETE:
    .delete(async (req:Request, res: Response) => {
        // Obtain a Query param (ID)
        let id: any = req?.query?.id;
        LogInfo(`Query Params: ${id}`);
        // Controller Instance to excute method
        const controller: UserController = new UserController();
        // Obtain Reponse
        const response: any = await controller.deleteUsers(id)
        // Send to the client the response
        return res.status(200).send(response);
    })


    .put(async (req:Request, res: Response) =>{
        // Obtain a Query param (ID)
        let id: any = req?.query?.id;
        let name: any = req?.query?.name;
        let age: any = req?.query?.age;
        let email: any = req?.query?.email;
        LogInfo(`Query Params: ${id}, ${name}, ${age}, ${email}`);

        // Controller Instance to excute method
        const controller: UserController = new UserController();

        let user = {
            name: name,
            email: email,
            age: age 
        }

        // Obtain Response
        const response: any = await controller.updateUser(id, user);

        // Send to the client the response
        return res.status(200).send(response);

    })



 

// Export Users Router
export default usersRouter;

/***
 * 
 * Get Documents => 200 OK
 * Creation Documents => 201 OK
 * Delete of Documents => 200 (Entity) / 204 (No return)
 * Update of Documents => 200 (Entity) / 204 (No return)
 * 
 * 
 */